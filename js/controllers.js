var nbcapiControllers = angular.module('nbcapiControllers', []);

nbcapiControllers.controller('NbcApiStoryCtrl', ['$scope', '$http', '$routeParams',
  function ($scope, $http, $routeParams) {
    $scope.loading = true;
    $http.get('/query/entries.php?taxonomy=' + $routeParams.taxonomy + '&slug=' + $routeParams.slug)
        .success(function(data) {
          $scope.loading = false;
          $scope.stories = data.entries;
        })
        .error(function(data) {
          console.log(data);
        });
    $scope.predicate = '-last_modified_utc';
    $scope.reverse = false;
    $scope.title = $routeParams.taxonomy + '|' + $routeParams.slug
    $scope.urlFilter = function(item) { return item.schema == 'OriginalUrl' }
  }
  ]);

nbcapiControllers.controller('NbcApiTopStoriesCtrl', ['$scope', '$http', '$routeParams',
  function ($scope, $http, $routeParams) {
    $scope.loading = true;
    get_top_stories($scope, $http);
    $scope.predicate = '-last_modified_utc';
    $scope.reverse = false;
    $scope.urlFilter = function(item) { return item.schema == 'OriginalUrl' }
  }
  ]);

nbcapiControllers.controller('NbcApiStorylineCtrl', ['$scope', '$http', '$routeParams',
  function ($scope, $http, $routeParams) {
    $scope.loading = true;
    get_schema($scope, $http, 'storyline');
    $scope.predicate = '-last_modified_utc';
    $scope.reverse = false;
    $scope.taxonomy = 'storyline';
  }
  ]);

nbcapiControllers.controller('NbcApiTopicCtrl', ['$scope', '$http', '$routeParams',
  function ($scope, $http, $routeParams) {
    $scope.loading = true;
    get_schema($scope, $http, 'topic');
    $scope.predicate = '-last_modified_utc';
    $scope.reverse = false;
    $scope.taxonomy = 'topic';
}
  ]);

nbcapiControllers.controller('NbcApiSectionCtrl', ['$scope', '$http', '$routeParams',
  function ($scope, $http, $routeParams) {
    $scope.loading = true;
    get_schema($scope, $http, 'section');
    $scope.predicate = '-last_modified_utc';
    $scope.reverse = false;
    $scope.taxonomy = 'section';
  }
  ]);

nbcapiControllers.controller('NbcApiSingleStoryCtrl', ['$scope', '$http', '$routeParams',
  function ($scope, $http, $routeParams) {
    $scope.loading = true;
    $http.get('/query/story.php?url=' + $routeParams.url)
        .success(function(data) {
          $scope.loading = false;
          $scope.story = eval(data).results[0];
        })
        .error(function(data) {
          console.log(data);
        });
    $scope.predicate = '-last_modified_utc';
    $scope.reverse = false;
    $scope.taxonomy = 'section';
  }
  ]);


var get_schema = function(scope, http, schema_type) {
  http.get('/query/schema.php?type=' + schema_type)
    .success(function(data) {
      scope.loading = false;
      scope.list_data = data.results;
    })
    .error(function(data) {
      console.log(data);
    });
}

var get_top_stories = function(scope, http) {
  http.get('/query/topstories.php')
    .success(function(data) {
      scope.loading = false;
      scope.stories = data.entries;
    })
    .error(function(data) {
      console.log(data);
    });
}

var foo = function(x) { return x; }