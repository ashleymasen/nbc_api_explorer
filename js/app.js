'use strict';

var nbcapiApp = angular.module('nbcapiApp', ['ngRoute', 'ui.bootstrap', 'nbcapiControllers'])

nbcapiApp.config(['$routeProvider',
    function($routeProvider) {
    	$routeProvider
            .when('/', {
                templateUrl: '/partials/topstories.html',
                controller: 'NbcApiTopStoriesCtrl'
            })
            .when('/topics', {
                templateUrl: '/partials/topics.html',
                controller: 'NbcApiTopicCtrl'
            })
            .when('/sections', {
                templateUrl: '/partials/sections.html',
                controller: 'NbcApiSectionCtrl'
            })
    		.when('/storylines', {
    			templateUrl: '/partials/storylines.html',
    			controller: 'NbcApiStorylineCtrl'
    		})
            .when('/stories/:taxonomy/:slug', {
                templateUrl: '/partials/entries.html',
                controller: 'NbcApiStoryCtrl'
            })
            .when('/story/:url*', {
                templateUrl: '/partials/story.html',
                controller: 'NbcApiSingleStoryCtrl'
            })
        .otherwise({ redirectTo: '/'});
      }
]);

nbcapiApp.filter('escape', function() {
  return window.escape;
});

nbcapiApp.filter('encodeURIComponent', function() {
    return window.encodeURIComponent;
});

